#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

lyrics = """Cause you're my fella, my guy
Hand me your stella and fly
By the time I'm out the door
You tear men down like Roger Moore

I cheated myself
Like I knew I would
I told ya, I was trouble
You know that I'm no good"""

class MyWindow(Gtk.Window):

	def __init__(self):
		super(MyWindow, self).__init__(title='This is fun!')

		self.init_ui()

	def init_ui(self):    
		
		self.modify_bg(Gtk.StateType.NORMAL, Gdk.Color(6400, 6400, 6440))
		self.set_border_width(10)
		
		self.set_icon_from_file("web.png")

		grid = Gtk.Grid()
		self.add(grid)
		
		
		
		label = Gtk.Label(lyrics)
		label.set_size_request(250,180)
		
		image = Gtk.Image()
		image.set_from_file('web.png')
		image.set_size_request(100,100)
		
		grid.attach(label, 0, 0, 1, 1)
		grid.attach(image, 1, 0, 2, 1)
		
		
		
		self.set_title("Mhmm")
		self.connect("destroy", Gtk.main_quit)


win = MyWindow()
win.show_all()
Gtk.main()
